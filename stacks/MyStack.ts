import { Api, NextjsSite, StackContext } from "@serverless-stack/resources";

export function MyStack({ stack, app }: StackContext) {
  // Create the HTTP API
  const api = new Api(stack, "Api", {
    routes: {
      "POST /send-otp": "functions/send-otp.handler",
    },
  });

  api.attachPermissions(["ses:SendEmail"]);

  // Create a Next.js site
  const site = new NextjsSite(stack, "Site", {
    path: "frontend",
    environment: {
      REGION: app.region,
      API: api.url,
    },
  });

  // Show the URLs in the output
  stack.addOutputs({
    SITE: site.url,
    API: api.url,
  });
}
