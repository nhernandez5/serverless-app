import { APIGatewayProxyEvent, APIGatewayProxyResult } from "aws-lambda";
import AWS from 'aws-sdk';

const ses = new AWS.SES();

export async function handler(event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> {
  const data = JSON.parse(event.body);
  const allowedEmails = [
    'nestor271828@gmail.com',
    'chris@thoughtfulautomation.com',
    'dan@thoughtfulautomation.com',
  ];

  if (!Object.prototype.hasOwnProperty.call(data, 'email')) {
    return {
      statusCode: 400,
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        msg: 'Please include an email',
      }),
    };

  }

  if (!allowedEmails.includes(data.email)) {
    return {
      statusCode: 403,
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        msg: 'Invalid email',
      }),
    };

  }

  const otp = Math.floor(100000 + Math.random() * 900000);
  const emailBody =
  `<!DOCTYPE html>
    <html>
      <body>
        <p>Use this code to verify your login at Simple OTP</p>
        <p><h1>${otp}</h1></p>
      </body>
    </html>`;

  const params = {
    Destination: {
      ToAddresses: [data.email],
    },
    Message: {
      Body: {
        Html: {
          Charset: 'UTF-8',
          Data: emailBody,
        },
      },
      Subject: {
        Charset: 'UTF-8',
        Data: "Your OTP from Buy me a tea serverless app",
      },
    },
    Source: `BMAT <nestor271828@gmail.com>`,
  };

  await ses.sendEmail(params).promise();

  // NOTE: should not return the code for security reasons but
  // due to time constraints, opted for this verification solution
  return {
    statusCode: 200,
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ code: otp }),
  };
};
