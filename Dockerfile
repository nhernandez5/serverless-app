FROM node:16-alpine

WORKDIR /home

EXPOSE 3000

ARG AWS_ACCESS_KEY
ARG AWS_SECRET_KEY

# Install packages
RUN apk update && apk add --update --no-cache \
    git \
    bash \
    curl \
    openssh \
    python3 \
    py3-pip \
    py-cryptography \
    wget \
    curl

RUN apk --no-cache add --virtual builds-deps build-base python3

# Update NPM
RUN npm config set unsafe-perm true
RUN npm update -g

# Install & setup AWS CLI
RUN pip install --upgrade pip && \
    pip install --upgrade awscli

RUN aws configure set aws_access_key_id $AWS_ACCESS_KEY
RUN aws configure set aws_secret_access_key $AWS_SECRET_KEY
