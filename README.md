# serverless-app

A proof-of-concept serverless app architecture that leverages the [SST Framework](https://sst.dev/) to
deploy a Next.js web app to S3 alongside AWS lambda functions. It humbly requests for tea via a valid email
address.

## Requirements
* [Node.js](https://nodejs.org/en/download/) v16

## Development flow

To provide a consistent developer work environment, it is recommended using the provided
container image.

You can install docker by following the instructions for your distribution on
[Docker's documentation](https://docs.docker.com/install/#supported-platforms).

To build the image with your AWS credentials, run:
```bash
docker build -t serverless-temp --build-arg AWS_ACCESS_KEY={YOUR_ACCESS_KEY} --build-arg AWS_SECRET_KEY={YOUR_SECRET_KEY} .
```

To instantiate a container:
```bash
docker run --name serverless-app -dit -v `pwd`:/home serverless-temp -p 3000:3000 /bin/sh
```

## Starting the Dev Environment
From this project root directory, start the SST app by running:

```node
$ npm start
```

Then, you should be able to start the Next.js development environment by running the following from the `frontend/` directory :

```node
$ npm run dev
```

The Next.js app should now be available at http://localhost:3000

## TODO
- Store OTP outside of the client
    - Requires more resources & setup (e.g. DB, key-value store etc.)
    - May require another lambda function for verification

- Break up states into separate components i.e. verification, success, etc.

- Improve docker setup i.e. use .env file or other best practices to pass sensitive info
such as AWS credentials