import React from 'react'
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';


export default function TeaForm({ isOpen, onClose, tea }) {
  const [email, setEmail] = React.useState('');
  const [code, setCode] = React.useState('');
  const [state, setState] = React.useState({
    otp: null,
    processing: false,
    sentOtp: false,
    failed: false,
    success: false,
    errorMsg: null,
  });

  const handleSendCode = React.useCallback(async () => {
    setState({
      otp: null,
      processing: true,
      failed: false,
      sentOtp: false,
      success: false,
      errorMsg: null,
    });

    const res = await fetch(
      '/api/send-otp',
      {
        method: 'POST',
        body: JSON.stringify({ email }),
      }
    );
    const data = await res.json();

    if (
      !res.ok
      || (res.status < 200 && res.status >= 300)
    ) {
      setState({
        otp: null,
        processing: false,
        sentOtp: false,
        failed: true,
        success: false,
        errorMsg: data.msg,
      });

    } else {
      setState({
        otp: data.code,
        processing: false,
        sentOtp: true,
        failed: false,
        success: false,
        errorMsg: null,
      });
    }
  }, [email, setState]);

  const handleSubmit = React.useCallback(() => {
    if (code == state.otp) {
      setState((prev) => ({
        ...prev,
        failed: false,
        success: true,
        errorMsg: null,
      }));

    } else {
      setState((prev) => ({
        ...prev,
        failed: true,
        success: false,
        errorMsg: 'Invalid Verification Code'
      }));
    }

  }, [code, state, setState]);

  const updateClientVerification = React.useCallback((val) => {
    setCode(val);
  }, [setCode]);

  const updateEmail = React.useCallback((val) => {
    setEmail(val);
  }, [setEmail]);


  return (
    <Dialog open={isOpen} onClose={onClose}>
      {
        state.success
          ? <>
            <DialogTitle sx={{ color: 'success.main' }}>Thanks for the {tea} tea!</DialogTitle>
            <DialogContent>
              <DialogContentText>
                I should be receiving it soon. Hopefully it doesn't cool down by the time it gets here.
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button disabled={state.processing} onClick={onClose}>Close</Button>
            </DialogActions>
          </>
          : <>
            <DialogTitle>Purchase</DialogTitle>
              <DialogContent>
                <DialogContentText>
                  Thanks to our kind donors at the CIA and NSA, this tea is 100% covered
                  so long as you provide a valid email address.
                </DialogContentText>
                <TextField
                  autoFocus
                  margin="dense"
                  id="name"
                  label="Email Address"
                  type="email"
                  fullWidth
                  variant="standard"
                  disabled={state.processing || state.sentOtp}
                  value={email}
                  onChange={(e) => {
                    updateEmail(e.target.value);
                  }}
                />
                {
                  state.sentOtp
                  && <TextField
                    autoFocus
                    margin="dense"
                    id="name"
                    label="Verification Code"
                    type="password"
                    fullWidth
                    variant="standard"
                    disabled={state.processing}
                    value={code}
                    onChange={(e) => {
                      updateClientVerification(e.target.value);
                    }}
                  />
                }
                {
                  state.failed
                    && (
                    <Typography
                      variant="subtitle1"
                      color="error"
                      align="center"
                      sx={{ fontFamily: 'secondary', textShadow: 'none' }}
                    >
                      {state.errorMsg ?? 'Error'}
                    </Typography>
                  )
                }
              </DialogContent>
              <DialogActions>
                <Button disabled={state.processing} onClick={onClose}>Cancel</Button>
                {
                  state.sentOtp
                  ? <Button
                    disabled={state.processing}
                    onClick={handleSubmit}
                    onKeyPress={handleSubmit}
                    sx={{ color: 'secondary.main' }}
                  >
                    Submit
                  </Button>
                  : <Button
                    disabled={!email || state.processing}
                    onClick={handleSendCode}
                    onKeyPress={handleSendCode}
                    sx={{ color: 'secondary.main' }}
                  >
                    Send Code
                  </Button>
                }
              </DialogActions>
          </>
      }
    </Dialog>
  );
}
