import { createTheme, responsiveFontSizes } from '@mui/material/styles';

let theme = createTheme({
  palette: {
    primary: {
      main: '#ecb531',
    },
    secondary: {
      main: '#b65b3e',
    },
    error: {
      main: '#f44336',
    },
    warning: {
      main: '#f9a10f',
    },
    info: {
      main: '#283f3f',
    },
    success: {
      main: '#7cb147',
    },
  },
});
theme = responsiveFontSizes(theme);

export default theme;
