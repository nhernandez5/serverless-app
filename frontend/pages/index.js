import React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import FormControl from '@mui/material/FormControl';
import FormHelperText from '@mui/material/FormHelperText';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import Stack from '@mui/material/Stack';
import Typography from '@mui/material/Typography';
import TeaForm from '../components/Form';
import styles from '../styles/Home.module.css'

export default function Home() {
  const [tea, setTea] = React.useState('');
  const [viewForm, setViewForm] = React.useState(false);

  const handleViewForm = () => {
    setViewForm(true);
  };

  const handleCloseForm = () => {
    setViewForm(false);
  };

  const handleSelect = (event) => {
    setTea(event.target.value);
  };

  return (
    <>
      <Box
        sx={{
          position: 'relative',
          height: '50vh',
          width: '100%',
          backgroundColor: 'primary.main',
          backgroundImage: 'url("/cup-o-joe.jpg")',
          backgroundPosition: 'center center',
          backgroundRepeat: 'no-repeat',
          backgroundSize: 'contain',
          objectFit: 'contain',
        }}
      />
      <Container maxWidth="md">
        <br />
        <Stack direction="row" justifyContent="center">
          <Typography variant="h2" color="text.main">
            Buy me a&nbsp;
          </Typography>
          <Typography variant="h2" color="secondary.main" sx={{ fontStyle: 'italic' }}>
            TEA
          </Typography>
        </Stack>
        <Typography variant="body2" color="text.secondary" align="center">
          Not much for coffee, but I do enjoy a nice cup of tea. If you are
          feeling charitable, select a tea from our options below and fill out the form
          when prompted. Theoretically, this tired dev would get a nice cup of tea on success. Thanks!
        </Typography>
        <FormControl required fullWidth sx={{ m: 1 }}>
          <InputLabel id="select-tea-label">Tea</InputLabel>
          <Select
            labelId="select-tea-label"
            id="select-tea"
            value={tea}
            label="Tea *"
            onChange={handleSelect}
          >
            <MenuItem value="">
              <em>None</em>
            </MenuItem>
            <MenuItem value="CHAMOMILE">Chamomile</MenuItem>
            <MenuItem value="EARL-GRAY">Earl Gray</MenuItem>
            <MenuItem value="ENGLISH-BREAKFAST">English Breakfast</MenuItem>
          </Select>
          <FormHelperText>Required</FormHelperText>
        </FormControl>
        <Box sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          height: '80px',
          width: '100%',
          '& > button': {
            height: '50px',
            width: '30%',
            minWidth: '185px',
            backgroundColor: 'secondary.main',
            color: '#F9F9F9',
          },
          '& > button:hover': {
            backgroundColor: 'secondary.dark',
            color: '#EEE',
          }
        }}
        >
          <Button variant="contained" disabled={!tea} onClick={handleViewForm}>
            PURCHASE
          </Button>
        </Box>
        <TeaForm isOpen={viewForm} onClose={handleCloseForm} tea={tea} />
      </Container>
      <footer className={styles.footer}>
        <Typography variant="body2" color="text.secondary" align="center">
          {`Copyright © nestor.dev ${new Date().getFullYear()}. All Rights Reserved.`}
        </Typography>
      </footer>
    </>
  )
}
