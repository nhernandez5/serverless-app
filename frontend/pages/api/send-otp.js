export default async function handler(req, res) {
  if (req.method !== 'POST') {
    res.setHeader('Allow', ['POST']);
    res.status(405).end(`Method ${req.method} Not Allowed`);
  }

  try {
    const body = JSON.parse(req.body);

    const sentOtpRes = await fetch(
      `${process.env.API}/send-otp`,
      {
        method: 'POST',
        body: JSON.stringify({
          email: body.email,
        }),
      }
    );

    const data = await sentOtpRes.json();

    res.status(sentOtpRes.status).json(data)

  } catch (error) {
    console.error(error);
    res.status(500).end('Unexpected error occured, failed to send otp');
  }
}
